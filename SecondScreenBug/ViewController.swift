//
//  ViewController.swift
//  SecondScreenBug
//
//  Created by Kiyotaka Sasaya on 12/15/14.
//  Copyright (c) 2014 LoiLo inc. All rights reserved.
//

import UIKit
import MediaPlayer

class ViewController: UIViewController {
    let player = MPMoviePlayerController(contentURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("movie", ofType: "mov")!))
    let windows = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        player.allowsAirPlay = false
        player.controlStyle = .Embedded
        player.view.frame = CGRectMake(0, 0, 320, 240)
        player.prepareToPlay()
        self.view.addSubview(player.view)
        
        var s = UISwitch(frame: CGRectMake(10, 260, 0, 0))
        self.view.addSubview(s)
        s.on = true
        s.addTarget(self, action: "valueChanged:", forControlEvents: .ValueChanged)
    }
    
    func valueChanged(sender: UISwitch) {
        if (sender.on) {
            for w in windows {
               (w as UIWindow).hidden = true;
            }
            windows.removeAllObjects()
        }
        else {
            for s in UIScreen.screens() {
                let screen = s as UIScreen
                if (screen.mirroredScreen != nil) {
                    let w = UIWindow(frame: CGRectZero)
                    w.screen = screen
                    let label = UILabel(frame: CGRectMake(0, 0, 100, 100))
                    label.text = "hello"
                    w.addSubview(label)
                    w.hidden = false
                    windows.addObject(w)
                }
            }
        }
    }
}

